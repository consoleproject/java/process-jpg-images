sample.jpg --- original, not reduced, not BASE64 encoded
sample_reduced.jpg --- processed 'sample.jpg', reduced, BASE64 encoded
sample_reduced_reduced.jpg --- processesed 'sample_reduced.jpg', reduced, not BASE64 encoded

!!!ATTENTION --- all processesed jpg images contain '_reduced' string concatenated to the end of the name, and those images are obtained by insignificant changes in the code regarding BASE64 encoding
