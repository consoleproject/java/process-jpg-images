package ProcessJPG;

import exceptions.NoFileSpecified;
import exceptions.NotBase64JPG;
import exceptions.NotValidFilePath;
import org.junit.jupiter.api.Test;

import javax.imageio.IIOException;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class ProcessJPGTest {
    @Test
    public void checkFirstConstruct(){
        try {
            ProcessJPG processJPG = new ProcessJPG();
        } catch (NoFileSpecified err){
            System.out.println(err.getMessage());
            assertEquals("JPG path is not specified for ProcessJPG.ProcessJPG", err.getMessage());
            System.out.println("checkFirstConstruct()      -- success");
        }
    }
    @Test
    public void checkSecondConstruct(){
        try{
            ProcessJPG processJPG = new ProcessJPG("src/test/resources/sample.jpg");
            assertFalse(processJPG.isNoPath());
            System.out.println("checkSecondConstruct()     -- success");
        } catch (NotValidFilePath err){
            System.out.println(err.getMessage());
            assertEquals("File path is not valid, please refer to the above message for details", err.getMessage());
            System.out.println("checkSecondConstruct()     -- success");
        }


    }

    @Test
    public void checkReduceJPGDimensions(){
        try {
            ProcessJPG processJPG = new ProcessJPG("src/test/resources/sample.jpg");
            processJPG.reduceJPGDimensions(0.01f); //lowest reduction
            assertFalse(processJPG.isNoPath());
        } catch (IOException err) {
            System.out.println(err.getMessage());
        } catch (NotValidFilePath err) {
            System.out.println(err.getMessage());
            assertEquals("File path is not valid, please refer to the above message for details", err.getMessage());
        } catch (NotBase64JPG err){
            System.out.println(err.getMessage());
            assertEquals("Specified image is not in BASE64", err.getMessage());
        }
    }


}