package exceptions;

public class NotBase64JPG extends Exception{
    public NotBase64JPG(String errorMessage){
        super(errorMessage);
    }
}
