package exceptions;

public class NoFileSpecified extends Exception{
    public NoFileSpecified(String errorMessage){
        super(errorMessage);
    }
}
