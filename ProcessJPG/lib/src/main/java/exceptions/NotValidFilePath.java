package exceptions;

public class NotValidFilePath extends Exception{
    public NotValidFilePath(String errorMessage){
        super(errorMessage);
    }
}